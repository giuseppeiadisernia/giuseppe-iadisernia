Giuseppe Iadisernia was born in Italy and grew up following horses and had aspirations of becoming a trainer. He has since helped many horses win races and resides in Florida while acting as a business owner for Romagnoli Productos Electricos Inc. Giuseppe is active with the Hollywood Spanish Seventh Day Adventist Church in Florida. He is active in helping the community by cooking and serving food among other initiatives.

Website : http://www.giuseppeiadisernia.com
